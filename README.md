# docker-eidas-build

This is a Docker image to build the eIDAS code and all the related projects
(e.g. FICEP applications/libraries).

## How to build it

Nothing more than

    $ make build

## How to use it (online scenario)

The container will clone the eIDAS code from a mirror repository.

    $ mkdir /tmp/eidas
    $ docker run -ti --rm -v "/tmp/eidas:/sources" eidas/build /bin/sh

Within the container...

    /sources # get-eidas-sources
    Cloning into '/sources/eidas'...
    remote: Counting objects: 4335, done.
    remote: Compressing objects: 100% (1758/1758), done.
    remote: Total 4335 (delta 2393), reused 4332 (delta 2393)
    Receiving objects: 100% (4335/4335), 19.51 MiB | 2.22 MiB/s, done.
    Resolving deltas: 100% (2393/2393), done.
    [+] -----------------------------------------------------
    [+] eIDAS sources cloned under /sources/eidas
    [+] -----------------------------------------------------
    /sources # cd /sources/eidas/EIDAS-Parent
    /sources/eidas/EIDAS-Parent # mvn clean install -P tomcat
    
    ... wait for Maven end ...

    [INFO] ------------------------------------------------------------------------
    [INFO] Reactor Summary:
    [INFO] 
    [INFO] eIDAS Node Parent .................................. SUCCESS [  5.507 s]
    [INFO] eIDAS Light Commons ................................ SUCCESS [ 20.608 s]
    [INFO] eIDAS Commons ...................................... SUCCESS [  7.107 s]
    [INFO] eIDAS Configuration Module ......................... SUCCESS [  6.359 s]
    [INFO] eIDAS Encryption ................................... SUCCESS [  8.552 s]
    [INFO] eIDAS SAML Engine .................................. SUCCESS [ 17.920 s]
    [INFO] eIDAS Updater ...................................... SUCCESS [  1.550 s]
    [INFO] eIDAS Specific Communication Definition ............ SUCCESS [  1.970 s]
    [INFO] eIDAS Specific ..................................... SUCCESS [  3.601 s]
    [INFO] eIDAS Node ......................................... SUCCESS [ 19.194 s]
    [INFO] eIDAS SP ........................................... SUCCESS [  8.501 s]
    [INFO] eIDAS IdP .......................................... SUCCESS [  2.506 s]
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 01:43 min
    [INFO] Finished at: 2017-10-25T15:11:01Z
    [INFO] Final Memory: 162M/425M
    [INFO] ------------------------------------------------------------------------
    /sources/eidas/EIDAS-Parent # exit

Back on the host...

    $ find /tmp/eidas -type f -iname "*.war"
    /tmp/eidas/eidas/EIDAS-Node/target/EidasNode.war
    /tmp/eidas/eidas/EIDAS-IdP-1.0/target/IdP.war
    /tmp/eidas/eidas/EIDAS-SP/target/SP.war

That's it!

## How to use it (offline scenario)

Let's assume that you already have all of your sources under `/tmp/eidas`

    $ ls -1 /tmp/eidas
    AdditionalFiles
    EIDAS-Commons
    EIDAS-Config
    EIDAS-ConfigModule
    EIDAS-Encryption
    EIDAS-IdP-1.0
    EIDAS-Light-Commons
    EIDAS-Node
    EIDAS-Parent
    EIDAS-SAMLEngine
    EIDAS-SP
    EIDAS-Specific
    EIDAS-SpecificCommunicationDefinition
    EIDAS-UPDATER

Run the Docker image as follow

    $ docker run -ti --rm -v "/tmp/eidas:/sources" eidas/build /bin/sh

Then (within the Docker) move to the Maven project dir (e.g. `EIDAS-Parent`)
and run `mvn` as you need

    /sources # cd EIDAS-Parent
    /sources/EIDAS-Parent # mvn clean install -P tomcat

At the end of the build process you can terminate the Docker and you will find
on your local file system all the generated files (e.g. `.war` archives).

    $ find /tmp/eidas -type f -iname "*.war"
    /tmp/eidas/EIDAS-Node/target/EidasNode.war
    /tmp/eidas/EIDAS-IdP-1.0/target/IdP.war
    /tmp/eidas/EIDAS-SP/target/SP.war

