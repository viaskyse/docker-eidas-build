build:
	docker build --tag eidas/build .

run: build
	docker run -ti --rm eidas/build /bin/sh
