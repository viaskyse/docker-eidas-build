#!/bin/sh
DEST_DIR="/sources/eidas"
git clone https://bitbucket.org/torsec/eidas-1.4.git ${DEST_DIR}
echo "[+] -----------------------------------------------------"
echo "[+] eIDAS sources cloned under ${DEST_DIR}"
echo "[+] -----------------------------------------------------"
